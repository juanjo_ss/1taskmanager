import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

/**
 * This class wraps all the request methods to make it more robust and provide the URL as a constant to use it
 * in the request methods and return the observables.
 * With this class we are moduling our code to be more efficient
 */
export class WebRequestService {

  readonly ROOT_URL;

  constructor(private http: HttpClient) {
    this.ROOT_URL = 'http://localhost:3000'; // A través de esta URL te estas conectando al backend de la aplicacion.
   }

   get(uri:string){
     return this.http.get(`${this.ROOT_URL}/${uri}`); // Template string `` return this observable
   }

   post(uri:string, payload:object){
     return this.http.post(`${this.ROOT_URL}/${uri}`, payload);
   }

   patch(uri:string, payload:object){
    return this.http.patch(`${this.ROOT_URL}/${uri}`, payload);
  }

  delete(uri:string){
    return this.http.delete(`${this.ROOT_URL}/${uri}`);
  }
}
