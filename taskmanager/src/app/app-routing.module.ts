import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskViewComponent } from './pages/task-view/task-view.component';
import { NewListComponent } from './pages/new-list/new-list.component';
import { NewTaskComponent } from './pages/new-task/new-task.component';
import { EditListComponent } from './pages/edit-list/edit-list.component';
import { EditTaskComponent } from './pages/edit-task/edit-task.component';


// Define the routes of each components which will present the wegpage
const routes: Routes = [
  { path:'', redirectTo: 'lists', pathMatch: 'full' },
  { path:'lists', component:TaskViewComponent },
  { path:'lists/:listID', component:TaskViewComponent },
  { path:'new-list', component:NewListComponent },
  { path:'lists/:listID/new-task', component:NewTaskComponent },
  { path:'edit-lists/:listID', component:EditListComponent },
  { path: 'lists/:listId/edit-task/:taskId', component: EditTaskComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
