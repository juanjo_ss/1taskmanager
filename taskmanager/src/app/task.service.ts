import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';
import { Task } from './models/task.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private webRequestService: WebRequestService) { }

  getLists() {
    return this.webRequestService.get('lists');
  }

  // We return the observable which we can treat on our controller side
  createList(title: string) {
    return this.webRequestService.post('lists', { title });
  }

  // We return the observable which we can treat on our controller side
  updateList(id: string, title: string) {
    return this.webRequestService.patch(`lists/${id}`, { title });
  }

  deleteList(id: string) {
    return this.webRequestService.delete(`lists/${id}`);
  }





  getTasks(listID: string) {
    return this.webRequestService.get(`lists/${listID}/tasks`);
  }

  createTask(title: string, listID: string) {
    // We want to send a web request to create a task
    return this.webRequestService.post(`lists/${listID}/tasks`, { title });
  }


  updateTask(listID: string, taskID: string, title: string) {
    // We want to send a web request to update a list
    return this.webRequestService.patch(`lists/${listID}/tasks/${taskID}`, { title });
  }

  deleteTask(listID: string, id: string) {
    return this.webRequestService.delete(`lists/${listID}/tasks/${id}`);
  }


  complete(task: Task) {
    // We toggle the completed stage by clicking on it will have the opposite value of the current completed value
    return this.webRequestService.patch(`lists/${task._listID}/tasks/${task._id}`, { completed: !task.completed });
  }

}
