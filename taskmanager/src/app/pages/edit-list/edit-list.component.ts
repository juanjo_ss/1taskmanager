import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TaskService } from 'src/app/task.service';

@Component({
  selector: 'app-edit-list',
  templateUrl: './edit-list.component.html',
  styleUrls: ['./edit-list.component.scss']
})
export class EditListComponent implements OnInit {

  constructor(private taskService: TaskService, private route: ActivatedRoute, private router: Router) { }

  selectedListID: string;

  ngOnInit() {
    // Get the route changes and will be returning the route parameters. Are really useful to get the value of the listID in the route.
    // As we .suscribe() to the observable whenever we click on the router link it will then 
    // notify the observable and get and update value of the listID
    this.route.params.subscribe(
      (params: Params) => {
        this.selectedListID = params.listID;
      }
    );
  }


  updateList(title:string) {
    this.taskService.updateList(this.selectedListID, title).subscribe(()=>{
      this.router.navigate(['/lists', this.selectedListID]); // Redirected to the main page and selected list (if not we will have to refresh the page to see the delete)
    })
  }
}
