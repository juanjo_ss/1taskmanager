import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task.model';
import { TaskService } from 'src/app/task.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss']
})
export class NewTaskComponent implements OnInit {

  constructor(private taskService:TaskService, private route: ActivatedRoute, private router: Router) { }

  listID: string;

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        // Set the propertie
        this.listID = params['listID'];
        console.log(this.listID);
      }
    )
  }

  createTask(title: string) {
    this.taskService.createTask(title, this.listID).subscribe((newTask: Task)=>{
      this.router.navigate(['../'], {relativeTo: this.route});
    });
  }
}
  