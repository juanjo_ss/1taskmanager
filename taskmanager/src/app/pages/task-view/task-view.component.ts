import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/task.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { List } from 'src/app/models/list.model';
import { Task } from 'src/app/models/task.model';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.scss']
})
export class TaskViewComponent implements OnInit {

  lists: List[];
  tasks: Task[];

  selectedListID: string;

  constructor(private taskService: TaskService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    // Get the route changes and will be returning the route parameters. Are really useful to get the value of the listID in the route.
    // As we .suscribe() to the observable whenever we click on the router link it will then 
    // notify the observable and get and update value of the listID
    this.route.params.subscribe(
      (params: Params) => {
          this.selectedListID = params.listID;
          this.taskService.getTasks(params.listID).subscribe((tasks:Task[])=>{
          this.tasks = tasks;
        })
      }
    )

    this.taskService.getLists().subscribe((lists: List[]) => {
      this.lists = lists;
      console.log(lists);
    })
  }

  onTaskClick(task: Task) {
    
    // We want to set the task to completed
    this.taskService.complete(task).subscribe(()=>{
      console.log("Completed succesfully");
      // The task has been set so completed succesfully
      // We toggle the completed stage by clicking on it will have the opposite value of the current completed value
      task.completed = !task.completed;
    })
  }

  onDeleteListClick() {
    this.taskService.deleteList(this.selectedListID).subscribe((response)=>{
      console.log(response);
      this.router.navigate(['/lists']); // Redirected to the main page (if not we will have to refresh the page to see the delete)
    });
  }

  onDeleteTaskClick(id:string) {
    this.taskService.deleteTask(this.selectedListID, id).subscribe((response)=>{
      this.tasks = this.tasks.filter(val => val._id !== id);
      console.log(response);
    });
  }
}

