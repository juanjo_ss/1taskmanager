const express = require('express');
const app = express();

const { mongoose } = require('./db/mongoose');
const bodyParser = require('body-parser');

/* Load-in Mongoose models */
const { List, Task } = require('./db/models');


/* ROUTES HANDLERS */

/* LIST ROUTES */


/* LOAD MIDDLEWARE */
app.use(bodyParser.json()); // This middleware will parse the request body of the HTTP request


// CORS HEADERS MIDDLEWARE
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    // We have a header to allow all the methods on HTTP
    res.header("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

/**
 * GET /lists
 * Purpose: Get all lists
 */
app.get('/lists', (req, res) => {
    // We want to return an array of the lists in the database
    List.find({}).then((lists) => {
        res.send(lists)
    })
});


/**
 * POST /lists
 * Purpose: Create lists
 */
app.post('/lists', (req, res) => {
    // We want to create a new list and return the new list document back to the user (which includes ID)
    // The lists information field will be past in via the JSON request body

    let title = req.body.title; // To create a new List we firstly have to read the data from the JSON request body

    let newList = new List({
        title
    });
    // The full list document is returned (ID included)
    newList.save().then((listDoc) => {
        res.send(listDoc);
    });
});


/**
 * PATCH /lists/:id
 * Purpose: Update an specific list
 */
app.patch('/lists/:id', (req, res) => {
    // We want to update the specific list (list document with ID in the URL) with the new values specified in JSON body format
    List.findOneAndUpdate({
        _id: req.params.id
    }, {
            $set: req.body // $set, MongoDB keyword, it will update the list that finds with the condition with the content of the request
        }).then(() => {
            res.send({message:'Updated succesfully'});
        });
});


/**
 * DELETE /lists/:id
 * Purpose: Delete an specific list
 */
app.delete('/lists/:id', (req, res) => {
    // We want to delete the specific list (list document with ID in the URL)
    List.findOneAndRemove({
        _id: req.params.id
    }).then((removeListDocument) => {
        res.send(removeListDocument);
    });
});


/* TASKS LISTS */

/**
 * GET /lists/:listID/tasks
 * Purpose: Get all the tasks of a list
 */
app.get('/lists/:listID/tasks', (req, res) => {

    Task.find({
        _listID: req.params.listID
    }).then((tasks) => {
        res.send(tasks)
    })
});


/**
 * POST /lists/:listID/tasks
 * Purpose: Create tasks for an specific list
 */
app.post('/lists/:listID/tasks', (req, res) => {

    let newTask = new Task({
        title: req.body.title,
        _listID: req.params.listID
    });


    // The full list document is returned (ID included)
    newTask.save().then((newTaskDoc) => {
        res.send(newTaskDoc);
    });
});


/**
 * PATCH /lists/:listID/tasks/:taskID
 * Purpose: Update an specific tasks specified by taskID
 */
app.patch('/lists/:listID/tasks/:taskID', (req, res) => {
    Task.findOneAndUpdate({
        _id: req.params.taskID,
        _listID: req.params.listID
    }, {
            $set: req.body // $set, MongoDB keyword, it will update the list that finds with the condition with the content of the request
        }).then(() => {
            res.send({message:'Updated succesfully'});
        });
});


/**
 * DELETE /lists/:listID/tasks/:taskID
 * Purpose: Delete a task from an specific list
 */
app.delete('/lists/:listID/tasks/:taskID', (req, res) => {
    // We want to delete the specific list (list document with ID in the URL)
    Task.findOneAndRemove({
        _id: req.params.taskID,
        _listID: req.params.listID
    }).then((removeTaskDocument) => {
        res.send(removeTaskDocument);
    });
});


/**
 * GET /lists/:listID/tasks/:taskID
 * Purpose: Get an specific task from a list
 */
app.get('/lists/:listID/tasks/:taskID', (req, res) => {
    Task.findOne({
        _id: req.params.taskID,
        _listID: req.params.listID
    }).then((task) => {
        res.send(task);
    });
});


app.listen(3000, () => {
    console.log("Server is listening in port 3000");
})
