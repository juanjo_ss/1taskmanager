const { List } = require('./list.model');
const { Task } = require('./task.model');


/* Exported both together so we can access each other through one require() */ 
module.exports = {
    List,
    Task
}