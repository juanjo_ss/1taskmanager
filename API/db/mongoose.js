// This file will handle the connecion logic to the MongoDB database

const mongoose = require('mongoose');

mongoose.Promise = global.Promise; // Gloabl JS promise instead 
mongoose.connect('mongodb://localhost:27017/TaskManager', {useNewUrlParser: true}).then(() => {
    console.log("Connected to MongoDB succesfully");
}).catch((e) => {
    console.log("Error while attempting to connect to MongoDB");
    console.log(e);
});

// To prevent deprecation warning (from MongoDB native driver)
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

// Export the mongoose object
module.exports = { mongoose };

// MongoDB not connecting issue on macOS Catalina, solved link:
// https://github.com/mongodb/homebrew-brew/issues/22