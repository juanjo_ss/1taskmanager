# Taskmanager App

This applicaction is a taskmanager app using MEAN Stack development.

The application consists of a taskmanager where you can have multiples Lists, and for each List you can create multiple Tasks.

Tasks can have to status completed and incompleted, to complete tasks you just have to point and click in it, it will be marked as completed and you will be able to delete clicking the trash can button. To modify a task it has to be uncompleted and you modify it by clicking on the modify button.

## Back End side

It consists of the following folders:
* **API**: Initilizates the server side providing all the CRUD operations methods and paths. Connect to the database and manage the data using express framework.
    * **db**: Models and db login connections.
    * **app.js**: Contains all the express logic to run the server side control with the CRUD operations HTTP methods (GET,POST,PATCH,DELETE).

## Front End side

It consists of the following folders:
* **Taskmanager/src/app**: Cliente side where is programmed the logic and specific usage of the application.
    * **app-routing-module.ts**: Define the components and routes paths.
    * **app.module.ts**: Declares and imports all the NgModules used.
    * The code is programmed to outsource the logic from the **web requests service** to the **web service** and then to the **web components**
        * **task.services.ts**: This task.service is responsible for modify all the data of the tasks
        * **web-request.service.ts**: This web-request.service is responsible of wrap all the requests HTTP methods and provide the URL as a constant to use it on the requests.
        * **/pages**: Folder where is all the app functionallity programmed.
        * **/models**: Tasks and Lists models collection.


